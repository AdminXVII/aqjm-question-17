fn main() {
    // Argument parsing
    let mut args = std::env::args();
    args.next();
    let lines = if let Some(l) = args.next() {
        if let Ok(l) = l.parse::<u32>() {
            l
        } else {
            eprintln!("Error could not parse line number as an integer");
            std::process::exit(1);
        }
    } else {
        eprintln!("USAGE: math <lines> <scale>");
        std::process::exit(1);
    };
    let scale = if let Some(s) = args.next() {
        if let Ok(s) = s.parse::<u32>() {
            s
        } else {
            eprintln!("Error could not parse resolution as an integer");
            std::process::exit(1);
        }
    } else {
        eprintln!("USAGE: math <lines> <scale>");
        std::process::exit(1);
    };
    let gap = 10 * scale;

    // Space between two points
    let dx = 5 * scale;
    let dy = (4.33 * scale as f32).round() as u32;
    println!("{} {}", dx, dy);

    // Compute the image dimensions
    let imgx = 2 * gap + (dx * ((4040 - 1) / lines)) / 2;
    let imgy = 2 * gap + dy * (lines - 1);
    println!(
        "Image with size {}x{}, with {} pixels",
        imgx,
        imgy,
        imgx * imgy
    );

    // Create a new ImgBuf with width: imgx and height: imgy
    let mut imgbuf = image::ImageBuffer::new(imgx, imgy);

    // Add a white background
    for pixel in imgbuf.pixels_mut() {
        *pixel = image::Rgb([std::u8::MAX; 3]);
    }

    // Add reference lines
    for x in 0..gap {
        let color = image::Rgb([255, 0, 0]);
        imgbuf.put_pixel(x, gap, color); // top left, horizontal
        imgbuf.put_pixel(gap, x, color); // top left, vertical
        imgbuf.put_pixel(imgx - x - 1, imgy - gap, color); // bottom right, horizontal
        imgbuf.put_pixel(imgx - gap, imgy - x - 1, color); // bottom right, vertical
    }

    // Add line between two points
    for x in 1..dx {
        use std::f64::consts::PI;
        imgbuf.put_pixel(x + gap, gap, image::Rgb([0, 255, 0])); // Between 2 rows
        imgbuf.put_pixel(
            gap + (((x as f64) * f64::cos(PI / 3.)) as u32),
            gap + (((x as f64) * f64::sin(PI / 3.)) as u32),
            image::Rgb([0, 0, 255]),
        ); // Between two diagonal points
    }

    // Number of points left to render
    let mut left = 2020;
    // Line number
    let mut l = 0;

    while left > 0 {
        // Alternate between line width
        let odd = l % 2 == 1;
        let x = gap + dx * l / 2;
        // Number of points in a line
        let line = u32::min(lines / 2 + if odd { 0 } else { lines % 2 }, left);

        for r in 0..line {
            let y = gap + dy * 2 * r + if odd { dy } else { 0 };
            imgbuf.put_pixel(x, y, image::Rgb([0; 3]));

            // Check that the border is respected
            assert!(x >= gap);
            assert!(y >= gap);
            assert!(x <= imgx - gap);
            assert!(y <= imgy - gap);
        }

        // Remove the used points
        left -= line;

        // Except on the last line, check that the rectangle width is filled
        if left != 0 {
            assert_eq!(
                2 * dy * (line - 1),
                imgy - 2 * gap
                    - if lines % 2 == 0 {
                        dy
                    } else if odd {
                        2 * dy
                    } else {
                        0
                    }
            );
        }
        l += 1;
    }
    // Check that the points fill the rectangle's height
    assert_eq!(gap + dx * (l - 1) / 2, imgx - gap);

    // Save the image, the format is deduced from the path
    imgbuf
        .save(format!("demo-math-{}-x{}.png", lines, scale))
        .unwrap();
}
