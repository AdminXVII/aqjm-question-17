# Solution au problème 17 de l'AQJM

Documents détaillants la proposition d'une meilleure solution au problème 17 du concours 2020 de l'AQJM

# Documents
 - question-17-aqjm.pdf contient la preuve
 - src/mains.rs contient un programme Rust générant les solutions
 - 4 fichiers de démonstation pour les dispositions (voir la preuve)
 - doc.md est le fichier source pour la preuve
