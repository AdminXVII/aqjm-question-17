---
title:
- Solution optimale au problème 17 du concours 2020 de l'AQJM
author:
- Xavier L'Heureux
date:
- Février 2020
fontfamily:
- charter
header-includes:
- \usepackage{multicol}
---

<!--\maketitle
\thispagestyle{empty}
\clearpage
\setcounter{page}{1}-->

# Question
Augustine désire aménager un verger de pommiers sur un terrain rectangulaire. Elle a reçu 2020 arbres qu'elle doit planter. Un système d'arrosage automatique impose que les arbres soient plantés sur les sommets d'un réseau régulier dont les mailles sont des triangles équilatéraux de côté égal à 5 mètres. De plus, aucun arbre ne doit être à moins de 10 mètres du bord du verger.

*Quelle est la surface minimale du terrain en m²?*

Si besoin est, on prendra 1,732 pour $\sqrt 3$ et on arrondira la réponse à l'entier le plus proche.

# Réponse officielle
50 995 m²

# Solution trouvée
50 959 m² dans une grille de 232,5 sur 219,18

# Démarche
Soit une grille d'arbre disposée selon l'arrangement présenté à la figure 1.

![Disposition des arbres](./assets/disposition.svg)

L'ensemble de 3 arbres à 5 m l'un de l'autre est un triangle équilatéral de côté 5. On peut en déduire que l'espace entre 2 lignes est de $5 \times \sin 60° = \frac{5 \sqrt 3}{2}$ m (soit environ 4,33 m) et l'espace entre 2 rangées est de 2,5 m.

Par conséquent, l'aire du rectangle, incluant la bordure de 10 m, est:

\begin{equation}
A = [2,5(r - 1) + 20] \times [4,33(l - 1) + 20]
\end{equation}

Pour un nombre $n$ de point à disposer dans $l$ lignes, $n - (n \bmod l)$ points rentrent dans $2 \lfloor \frac{n}{l} \rfloor$ rangées. Les $n \bmod l$ points restants occupent $\left\lceil \dfrac{n \bmod l}{\frac{l}{2}} \right\rceil$ rangées. Simplifions:

\begin{equation}
\begin{aligned}
r &= 2 \left\lfloor \frac{n}{l} \right\rfloor + \left\lceil \dfrac{n \bmod l}{\frac{l}{2}} \right\rceil \\
&= 2 \frac{n - n \bmod l}{l} + \left\lceil 2\dfrac{n \bmod l}{l} \right\rceil \\
&= \left\lceil 2 \frac{n - n \bmod l}{l} + 2\dfrac{n \bmod l}{l} \right\rceil \\
&= \left\lceil 2 \frac{n - n \bmod l + n \bmod l}{l} \right\rceil \\
&= \left\lceil \frac{2n}{l} \right\rceil \\
\end{aligned}
\end{equation}

Par conséquent, il faut trouver le nombre $l$ de lignes qui minimise l'équation 1, sachant que $0 < l < 4040$.

\begin{equation}
\begin{aligned}
\min_l A &= \min_l [2,5(r - 1) + 20] \times [4,33(l - 1) + 20] \\
       &= \min_l [2,5(\left\lceil \frac{4040}{l} \right\rceil - 1) + 20] \times [4,33(l - 1) + 20]
\end{aligned}
\end{equation}

Certaines valeurs d'une recherche exhaustive effectuée sont présentées ci-dessous:

l   A
-- ------
35 51420
36 51465
37 51445
38 51360
39 51210
40 \colorbox{BurntOrange}{50995}
41 51198
42 51358
43 50970
44 51032
45 51051
46 51027
47 \colorbox{BurntOrange}{50959}
48 51407
49 51264
50 51077
51 51439
52 51176
53 51484
54 51145

On peut voir qu'effectivement, à l = 40, la solution donnée est bien  de 50995 m². Cependant, c'est une solution sous-optimale, puisqu'à l = 47, l'aire est bel et bien de 50959 m².

# Visualisation
Un programme faisant une simulation itérative de la plantation des arbres est disponible au <https://gitlab.com/AdminXVII/aqjm-question-17/>. Le code est documenté et devrait être facile à lire.

4 fichiers sont également fournis:

 - 2 à une précision de 6 pixels par mètre ($4,33 \approx \frac{26}{6}$) pour visualiser la solution:
     - [demo-math-40-x6.png](./demo-math-40-x6.png) qui présente la solution officielle
     - [demo-math-47-x6.png](./demo-math-47-x6.png) qui présente la solution optimale
 - 2 à une précision de 100 pixels par mètre pour comparer l'aire en pixel avec la solution théorique. Les aires concordent.
     - [demo-math-40-x100.png](./demo-math-40-x100.png) qui présente la solution officielle
     - [demo-math-47-x100.png](./demo-math-47-x100.png) qui présente la solution optimale
