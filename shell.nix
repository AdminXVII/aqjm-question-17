with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "env";
  buildInputs = [
    pandoc texlive.combined.scheme-full gnome3.librsvg
  ];
}

